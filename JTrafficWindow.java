package com.fredollinger;

import javax.swing.*;
import javax.swing.event.*;

import java.awt.*;
import java.awt.Event.*;
 
class JTrafficWindow extends JFrame {
    public JTrafficWindow(){
/*
	JTrafficCanvas canvas = new JTrafficCanvas();
	canvas.setBackground(Color.orange);
	JFrame frame = new JFrame( "Hello, Java" );
	frame.add(canvas);
*/
	JTrafficCanvas canvas = new JTrafficCanvas();
	canvas.setBackground(Color.orange);

	JFrame frame = new JFrame( "Hello, Java" );
	frame.add(canvas);
	frame.setSize( 600, 400 );
	frame.setVisible( true );
    } // END JTrafficWindow()
} // END class JTrafficWindow
